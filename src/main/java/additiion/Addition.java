package additiion;

/**
 * 
 * @author medbozo
 *
 *<i>Cette Classe fait la somme de deux entiers</i>
 */
public class Addition {
     /**
      * <i> Additionner deux entiers</i>
      * 
      * @param a
      *      <i>Premier entier<i>
      * @param b
      *      <i>Second entier</i>
      * @return
      *    <i>La somme des entiers re�us en param�tre</i>
      */
	 public int additionner(int a,int b) {
		 return (a+b);
	 }
}
